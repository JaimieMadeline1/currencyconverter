var express = require("express");
var app = express();
var cors = require("cors");

const hostname = "localhost";
const port = 3003;

app.use(cors());

// Constants
var currentRate = 2;


// Helpers
const returnRandomValue = function (max, min) {
    return Math.random() * (max - min) + min;
}

const returnNewRate = function () {
    return 2 + returnRandomValue(1, 0.01);
}

const startRandomRateGen = function() {
    currentRate = returnNewRate();

    const infiniteRateGen = function () {
        setTimeout(function () {    
            currentRate = returnNewRate();
            io.emit('SEND_CURRENT_RATE', JSON.stringify({ currentRate: currentRate }));

            infiniteRateGen();                      
        }, 100)
    }

    infiniteRateGen();
}

// Routes
app.get("/convertToUSD", function(req, res) {
    if (req.query && req.query.inputValue) {
        var value = req.query.inputValue;
        var result = value ? value * currentRate : 0;
        res.setHeader('Content-Type', 'application/json');
        res.status(200);
        res.end(JSON.stringify({ originalValue: value + "", convertedValue: result + "" }));
    } else {
        res.status(400);
        res.end("no input value found");
    }
});

// Socket implementation
var http = require('http').Server(app);
var io = require('socket.io')(http);

http.listen(8080, function(){
    console.log('listening on 8080');
});

io.on('connection', () =>{
    console.log('a user is connected')
})

// Start Nodejs Server
app.listen(port, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
    startRandomRateGen();
});
