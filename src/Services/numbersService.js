const isValidNumberOrEmpty = (num) => {
    return num === "" || /^\d+$/.test(num);
}

export { isValidNumberOrEmpty };