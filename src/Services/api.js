import Axios from "axios";

const convertToUSD = (inputValue) => {
    return Axios.get('http://localhost:3003/convertToUSD?inputValue=' + (inputValue ? inputValue : '0'))
}

export { convertToUSD };