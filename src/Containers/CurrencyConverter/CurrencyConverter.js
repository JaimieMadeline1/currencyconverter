import React, { Component } from 'react';
import { currencies } from '../../Const/currencies';
import styles from './CurrencyConverter.module.scss';
// import CurrencyDropdown from '../../Components/CurrencyDropdown/CurrencyDropdown';
import InputHelper from '../../Components/InputHelper/InputHelper';
import DropdownHelper from '../../Components/DropdownHelper/DropdownHelper';
import ConvertedValueDisplay from '../../Components/ConvertedValueDisplay/ConvertedValueDisplay';
import { isValidNumberOrEmpty } from '../../Services/numbersService';
import { convertToUSD } from '../../Services/api';
import io from "socket.io-client";

class CurrencyConverter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedCurrency: currencies.CAD, // default currency set to CAD
            inputValue: 0,
            displayValue: 0,
            currentRate: ''
        }

        this.currencyList = [
            { text: currencies.CAD.shortHand, value: currencies.CAD },
            { text: currencies.GBP.shortHand, value: currencies.GBP },
            { text: currencies.EUR.shortHand, value: currencies.EUR }
        ];

        this.socket = io('http://127.0.0.1:8080');
    }
    componentDidMount() {
        this.socket.on('SEND_CURRENT_RATE', (data) => {
            if (this.updateCurrentRate) {
                this.updateCurrentRate(data);
            }
        })
    }
    componentWillUnmount() {
        this.socket.removeAllListeners();
    }
    updateCurrentRate(data) {
        if (data) {
            let parsedJson = JSON.parse(data);
            this.setState({ currentRate: parsedJson.currentRate });
        }
    }
    convertInputValue = (inputValue) => {
        // use a service to filter out anything that's not a number and value has to be bigger than or equal to 0
        if (isValidNumberOrEmpty(inputValue) && inputValue !== this.state.inputValue) {
            let isEmptyStringValue = inputValue === '' || inputValue === '0';
            this.setState(isEmptyStringValue ? { inputValue: inputValue, displayValue: '0' } : { inputValue: inputValue });
            if (!isEmptyStringValue) {
                convertToUSD(inputValue)
                    .then((res) => {
                        console.log("res");
                        console.log(res);
                        console.log(inputValue);
                        let data = res.data;
                        console.log(data.originalValue);
                        if (inputValue === data.originalValue) {
                            this.setState({ displayValue: data.convertedValue });
                        }
                    })
                    .catch((err) => {
                        console.error(err);
                    })
            }
        }
    }
    changedCurrency = (newCurrency) => {
        if (newCurrency !== this.state.selectedCurrency) {
            this.setState({ selectedCurrency: newCurrency });
        }
    }
    render() {
        return (
            <div className={['row', styles.currencyContainer].join(' ')}>
                <h2 className={["col-md-12", styles.headerText].join(' ')}>Single dealer platform / application which will allow users to make foreign currency trades.</h2>
                <div className="col-md-3"><DropdownHelper dropdownBaseTitle={'Currency List'} itemList={this.currencyList}
                    changedDropdownValue={(newDropdownItem) => this.changedCurrency(newDropdownItem)} /></div>
                <div className="col-md-2">{this.state.selectedCurrency.shortHand}:</div>
                <div className="col-md-7">
                    <span><InputHelper defaultValue={0} symbol={this.state.selectedCurrency.symbol} value={this.state.inputValue}
                        changedInputValue={(changedInputValue) => this.convertInputValue(changedInputValue)} /></span>
                </div>
                <div className={["col-md-12", styles.currencyDisplayContainer].join(' ')}><ConvertedValueDisplay displayValue={this.state.displayValue}></ConvertedValueDisplay></div>
                <div className={["col-md-12", styles.currencyDisplayContainer].join(' ')}>Current Rate: {this.state.currentRate}</div>
            </div>
        );
    }
}

export default CurrencyConverter;
