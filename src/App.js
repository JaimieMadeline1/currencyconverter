import React, { Component } from 'react';
import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import CurrencyConverter from './Containers/CurrencyConverter/CurrencyConverter';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
            <CurrencyConverter></CurrencyConverter>
        </div>
      </div>
    );
  }
}

export default App;
