import React from 'react';
import { Dropdown } from 'react-bootstrap';

class DropdownHelper extends React.PureComponent {
    render() {
        return (
            <div>
                <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                        {this.props.dropdownBaseTitle}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        {this.props.itemList.map((dropdownItem) => {
                            return <Dropdown.Item key={dropdownItem.text} onClick={() => this.props.changedDropdownValue(dropdownItem.value)}>{dropdownItem.text}</Dropdown.Item>
                        })}
                    </Dropdown.Menu>
                </Dropdown>
            </div>
        );
    }
}

export default DropdownHelper;
