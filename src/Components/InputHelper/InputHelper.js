import React from 'react';
import styles from './InputHelper.module.scss';
import { InputGroup, FormControl } from 'react-bootstrap';

class InputHelper extends React.PureComponent {
    render() {
        return (
            <div>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text>{this.props.symbol}</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl value={this.props.value} onChange={(e) => this.props.changedInputValue(e.target.value)} aria-label="value" />
                    <InputGroup.Append>
                        <button className={styles.clearButton} onClick={() => this.props.changedInputValue(this.props.defaultValue + "")}>X</button>
                    </InputGroup.Append>
                </InputGroup>
            </div>
        );
    }
}

export default InputHelper;
