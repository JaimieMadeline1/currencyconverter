import React from 'react';
import { Badge } from 'react-bootstrap';

class ConvertedValueDisplay extends React.PureComponent {
    render() {
        return (
            <React.Fragment>
                {this.props.displayValue >= 0 && <div>Converted Value: <Badge variant="secondary">{this.props.displayValue}</Badge></div>}
            </React.Fragment>
        );
    }
}

export default ConvertedValueDisplay;
