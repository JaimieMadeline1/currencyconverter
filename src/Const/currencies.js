export const currencies = {
    'CAD': { shortHand: 'CAD', symbol: '$'},
    'GBP': { shortHand: 'GBP', symbol: '%'},
    'EUR': { shortHand: 'EUR', symbol: '#'},
};